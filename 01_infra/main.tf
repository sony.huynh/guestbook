
resource "aws_key_pair" "webserver-key" {
  key_name   = "webserver-key"
  public_key = file("/tmp/ssh.pub")
}

resource "aws_instance" "dev-webserver01" {
  ami                         = "ami-0df7a207adb9748c7"
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.webserver-key.key_name
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.dev-sg.id]
  subnet_id                   = "subnet-016c07d4a46596ee9"
  tags = {
    Name = "webserver01"
    Env = "dev"
  }
}

resource "aws_instance" "dev-redis01" {
  ami                         = "ami-0df7a207adb9748c7"
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.webserver-key.key_name
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.dev-sg.id]
  subnet_id                   = "subnet-016c07d4a46596ee9"
  tags = {
    Name = "redis01"
    Env = "dev"
  }
}
