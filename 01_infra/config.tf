terraform {
  required_version = ">= 1.1.9"

  backend "s3" {
    region         = "ap-southeast-1"
    bucket         = "do-vb-terraform-usrconfig"
    key            = "state/terraform.tfstate"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.35.0"
    }
  }
}


provider "aws" {
  region = "ap-southeast-1"
}

